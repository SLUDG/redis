package com.sludg.util.redis

import com.sludg.util.config.Configuration
import com.typesafe.config.Config
import redis.clients.jedis.{Jedis, JedisPool, JedisPoolConfig}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

/**
 * A companion object to hold reference to our RedisClient Configuration
 */
object RedisClient {
  lazy val redisConfig: Config = Configuration.config.getConfig("com.sludg.util.redis")
}

trait RedisClient {

  import RedisClient._

  /**
   * The Redis host to connect to
   */
  val host: String

  /**
   * The Redis password, if set.
   */
  val password: Option[String]

  /**
   * A JedisPool to get Jedis resources from, lazily instantiated.
   */
  lazy val jedisPool: JedisPool = password match {
    case Some(p) => new JedisPool(jedisPoolConfig, host, port, timeOut, p)
    case None => new JedisPool(jedisPoolConfig, host, port, timeOut)
  }

  /**
   * The port of the Redis instance to connect to <br>
   *  Default: 6379 <br>
   *  ENV: REDIS_PORT
   */
  val port: Int = redisConfig.getInt("port")

  /**
   * The default timeout for Redis (ms) <br>
   *  Default: 2000 <br>
   *  ENV: REDIS_TIMEOUT
   */
  val timeOut: Int = redisConfig.getInt("timeOut")

  /**
   * The minimum number of Idle Connections to maintain <br>
   *  Default: 25 <br>
   *  ENV: REDIS_MIN_IDLE
   */
  val minIdleConnections: Int = redisConfig.getInt("minIdleConnections")

  /**
   * The maximum number of Idle Connections to maintain <br>
   *  Default: 100 <br>
   *  ENV: REDIS_MAX_IDLE
   */
  val maxIdleConnections: Int = redisConfig.getInt("maxIdleConnections")

  /**
   * The maximum number of connections allowed <br>
   *  Default: -1 (Inf) <br>
   *  ENV: REDIS_MAX_CONNECTIONS
   */
  val maxConnections: Int = redisConfig.getInt("maxConnections")

  /**
   * Flag to test connections when returned to the connection pool <br>
   *  Default: true <br>
   *  ENV: REDIS_TEST_ON_RETURN
   */
  val testConnectionOnReturn: Boolean = redisConfig.getBoolean("testConnectionOnReturn")

  /**
   * Flag to test idle connections <br>
   *   Default: true <br>
   *   ENV: REDIS_TEST_WHILE_IDLE
   */
  val testConnectionWhileIdle: Boolean = redisConfig.getBoolean("testConnectionWhileIdle")

  /**
   * How long a connection has to be idle before being testing (ms)<br>
   *   Default: 300000 <br>
   *   ENV: REDIS_EVICT_MILLIS
   */
  val softMinEvictableIdleTimeMillis: Int = redisConfig.getInt("softMinEvictableIdleTimeMillis")

  /**
   * The redis connection pool. Default settings from reference.conf are: <br>
   *   25 minimum connections in the pool. <br>
   *   100 max connections in the pool. <br>
   *   Unlimited connections on burst, if needed. <br>
   *   Test connection on return to the pool. <br>
   *   Test connections while idle. <br>
   *   Evict members of the connection pool that have been idle for 5 minutes.
   */
  val jedisPoolConfig: JedisPoolConfig = {
    val jpc: JedisPoolConfig = new JedisPoolConfig()
    jpc.setMinIdle(minIdleConnections)
    jpc.setMaxIdle(maxIdleConnections)
    jpc.setMaxTotal(maxConnections)
    jpc.setTestOnReturn(testConnectionOnReturn)
    jpc.setTestWhileIdle(testConnectionWhileIdle)
    jpc.setSoftMinEvictableIdleTimeMillis(softMinEvictableIdleTimeMillis)
    jpc
  }

  /**
   * A nullary function to wrap a redis action to a Try[Option[T]]
   * @param thunk
   * @tparam T
   * @return
   */
  def tryAction[T](thunk: Jedis => T): Try[Option[T]] = Try {

    val jedis: Jedis = jedisPool.getResource

    val result: Option[T] = try {
      val result: T = thunk(jedis)
      Option(result)
    } catch {
      case _ : Exception => None
    }
    finally {
      jedis.close()
    }

    result
  }

  /**
   * A nullary function that calls tryAction and wraps in in a Future
   * @param thunk
   * @tparam T
   * @return
   */
  def tryActionAsync[T](thunk: Jedis => T): Future[Try[Option[T]]] = {
    Future {
      tryAction(thunk)
    }
  }

  /**
   * A nullary function to wrap a redis action to a Try[Option[T]] and then indiscriminately flatten it to Option[T]
   * @param thunk
   * @tparam T
   * @return
   */
  def optAction[T](thunk: Jedis => T): Option[T] = tryAction(thunk).toOption.flatten

  /**
   * A nullary function that calls optAction and wraps in in a Future
   * @param thunk
   * @tparam T
   * @return
   */
  def optActionAsync[T](thunk: Jedis => T): Future[Option[T]] = {
    Future {
      optAction(thunk)
    }
  }

  /**
   * Try to nicely shut down the connection pool when the VM exits
   */
  sys.addShutdownHook {
    if (!jedisPool.isClosed) {
      jedisPool.close()
    }
  }

}
