name := "redis"
organization := "com.sludg.util"
version := "0.1"

scalaVersion := "2.13.0"
crossScalaVersions := Seq("2.12.10", "2.11.12")

resolvers += "SLUDG@GitLab" at "https://gitlab.com/api/v4/groups/SLUDG/-/packages/maven"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.8" % Test,
  "com.sludg.util" %% "config" % "0.2",
  "redis.clients" % "jedis" % "3.1.0"
)

enablePlugins(GitlabPlugin)
com.sludg.sbt.gitlab.GitlabPlugin.gitlabProjectId := "14282192"